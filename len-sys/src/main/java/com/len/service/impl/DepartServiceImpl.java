package com.len.service.impl;

import com.len.base.impl.BaseServiceImpl;
import com.len.entity.SysDepart;
import com.len.mapper.SysDepartMapper;
import com.len.service.DepartService;
import org.springframework.stereotype.Service;

@Service
public class DepartServiceImpl extends BaseServiceImpl<SysDepartMapper,SysDepart> implements DepartService {

}
